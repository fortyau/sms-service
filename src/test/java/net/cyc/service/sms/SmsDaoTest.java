package net.cyc.service.sms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.junit4.SpringRunner;

import net.cyc.service.sms.dao.ISmsDao;
import net.cyc.service.sms.dao.SmsDao;
import net.cyc.service.sms.domain.SmsConfig;


@RunWith(SpringRunner.class)
public class SmsDaoTest {

	private EmbeddedDatabase embeddedDatabase;
	private ISmsDao dao; 
	
	
	@Before
	public void setUp() throws Exception {
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("create_test_database.sql")
                .build();
        
        dao = new SmsDao(new JdbcTemplate(embeddedDatabase));
	}
	
	@Test
	public void testSmsDao() throws SQLException {
		assertNotNull(dao);
	}

	@Test
	public void testGetSmsConfiguration() throws SQLException {
		Long id = 1L;
		SmsConfig daoConfig = dao.getSmsConfiguration(id);
		assertEquals("415-599-2671", daoConfig.getCallerId());
	}		

}
