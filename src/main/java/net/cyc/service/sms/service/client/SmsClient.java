package net.cyc.service.sms.service.client;

import java.util.Map;

import net.cyc.service.sms.domain.Constants;
import net.cyc.service.sms.domain.Response;
import net.cyc.service.sms.domain.SmsConfig;

public class SmsClient implements ISmsClient {

	private ISmsClient smsClient;
	
	
	public SmsClient(SmsConfig smsConfig, String clientType) {
		if(clientType.equalsIgnoreCase(Constants.TWILIO_CLIENT)) {
			this.smsClient = new TwilioClient(smsConfig);
		} else if(clientType.equalsIgnoreCase(Constants.AWS_CLIENT)) {
			this.smsClient = new AwsClient(smsConfig);			
		}
		else {
			this.smsClient = null;
		}
	}
	
	@Override
	public Response request(String url, String arg1, Map arg2) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public ISmsClient getSmsClient() {
		return smsClient;
	}

	public void setSmsClient(ISmsClient smsClient) {
		this.smsClient = smsClient;
	}

}
