package net.cyc.service.sms.service;

import java.util.List;

import net.cyc.service.sms.domain.NotificationSms;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.domain.SmsResponse;

public interface ISmsService {

	/**
	 * 
	 * @throws Exception
	 */
	public String sendSmsMessage(NotificationSms sms);
	
	/**
	 * 
	 * @return
	 */
	public OutboundTextMessage getOutboundTextMessage();
	
	/**
	 * 
	 * @param outboundTextMessage
	 */
	public void setOutboundTextMessage(OutboundTextMessage outboundTextMessage);

	public List<OutboundTextMessage> getAllSms();

}