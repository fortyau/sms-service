package net.cyc.service.sms.service.client;

import java.util.Map;

import net.cyc.service.sms.domain.Response;

public interface ISmsClient {
	
	public Response request(String url, String arg1, Map arg2) throws Exception;

}
