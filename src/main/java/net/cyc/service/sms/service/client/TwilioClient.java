/**
 * 
 */
package net.cyc.service.sms.service.client;

import java.util.Map;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestResponse;

import net.cyc.service.sms.domain.Response;
import net.cyc.service.sms.domain.SmsConfig;

/**
 * @author Jeff Corbett
 *
 */
public class TwilioClient implements ISmsClient {

	private TwilioRestClient twilioClient;
	
	private TwilioRestResponse twilioResponse;
	
	private Response response; 
	
	/*
	 * these are the constants that are currently being used
	 * not sure if these are duplicated in the DB
	 * 
	public static final String APIVERSION = "2010-04-01";
	public static final String ACCOUNTSID = "AC219cba661b25b91c8e975750cfb35d90";
	public static final String AUTHTOKEN = "4e8fc17bd9f50468886765d7ae28644f";
    private static final String CALLER_ID = "415-599-2671";
	 */
	
	
	public TwilioClient(SmsConfig config) {
		this.twilioClient = new TwilioRestClient(config.getAccountSid(), config.getAuthToken(), null);
	}
	
	@Override
	public Response request(String url, String methodType, Map params) throws Exception {		
		this.twilioResponse = this.twilioClient.request(url, methodType, params);
		this.response = new Response(twilioResponse.getResponseText(), 
				twilioResponse.getHttpStatus(), 
				twilioResponse.getUrl(), 
				twilioResponse.getQueryString());
		return response;
	}

	public TwilioRestClient getTwilioClient() {
		return twilioClient;
	}

	public TwilioRestResponse getTwilioResponse() {
		return twilioResponse;
	}

	public Response getResponse() {
		return response;
	}

}
