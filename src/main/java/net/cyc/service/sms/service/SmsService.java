/**
 * 
 */
package net.cyc.service.sms.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import net.cyc.service.sms.dao.ISmsDao;
import net.cyc.service.sms.domain.Constants;
import net.cyc.service.sms.domain.Content;
import net.cyc.service.sms.domain.NotificationSms;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.domain.Response;
import net.cyc.service.sms.domain.SmsConfig;
import net.cyc.service.sms.domain.SmsResponse;
import net.cyc.service.sms.exception.SmsServiceException;
import net.cyc.service.sms.service.client.ISmsClient;
import net.cyc.service.sms.service.client.SmsClient;
import net.cyc.service.sms.service.client.TwilioClient;
import net.cyc.service.util.ContentUtil;

/**
 * the previous way for this class to get statuses was to pass a long and use
 * that as a key to call hibernate and get the value from the table. going to
 * trim this down a bit.
 * 
 * @author Jeff Corbett
 *
 */
public class SmsService implements ISmsService {
	private static final long serialVersionUID = -7977124834752724958L;
	
	


	// Outbound message to populate for sending
	private OutboundTextMessage outboundTextMessage;

	// smsDao data access object
	private ISmsDao smsDao;
	
	private Long id = 1L; //TODO this is temp. will come up with logic for default value

	private static final Logger LOGGER = LoggerFactory.getLogger(SmsService.class);
	
	private SmsConfig smsConfig;
	
	@Value ("${sms.clientUrl}")
	private String clientUrl;
	
	private String smsClient;

	// default constructor with a dao injected
	public SmsService(ISmsDao smsDao, String clientUrl,String smsClient) {
		this.smsDao = smsDao;
		this.clientUrl = clientUrl;
		this.smsClient = smsClient;
	}

	


	/**
	 * This method takes a {@link NotificationSms} from the controller
	 * and tries to send an SMS to the recipient. whether or not it succeeds
	 * the message will be persisted, in order to try and send again if it
	 * failed.
	 * 
	 * @param NotificationSms
	 */
	@Override
	public String sendSmsMessage(NotificationSms sms)  {
		try {
			Objects.requireNonNull(sms.getPhoneNum(),"phone number cannot be empty");
			Objects.requireNonNull(sms.getNotificationType(),"Notification Type cannot be empty");
			this.outboundTextMessage = new OutboundTextMessage();
			this.outboundTextMessage.setToPhone(sms.getPhoneNum());
			this.outboundTextMessage.setMessageType(Constants.SMSMT_SAVE);
			
			this.outboundTextMessage.setSmsBody(populateBodyContent(sms));
			
			smsConfig = smsDao.getSmsConfiguration(id);		
			this.outboundTextMessage.setFromPhone(smsConfig.getCallerId());
			outboundTextMessage.setAccountSid(smsConfig.getAccountSid());
			outboundTextMessage.setStatus(Constants.SMS_STATUS_UNSENT);
			
			//populated the outbound, now get ready to send it out
			postTextMessage();
			SmsResponse response = new SmsResponse(true, "Sms status: " + outboundTextMessage.getStatus());

			return response.toString();
		} catch(Exception e) {
			LOGGER.error("SmsService.sendSmsMessage() threw error. ", e);
			throw new RuntimeException(e);
		}
		finally {
			try {
				if(this.outboundTextMessage.getStatus().equals(Constants.SMS_STATUS_SENT)) {
					outboundTextMessage.setSendDate(new Timestamp(System.currentTimeMillis()));
				}
				smsDao.persistOutboundMessage(this.outboundTextMessage);
			} catch (SQLException e) {
				LOGGER.error("Error occured in SmsDao.PersistOutboundTextMessage(): ", e);
			}
		}

	}
	

	/**
	 * leveraging adapter pattern to facilitate the actual sending of an SMS message
	 * this way if required, we can switch clients with just a flip of a 
	 * configuration.
	 * 
	 * @param smsMessage
	 * @return
	 */
	private void postTextMessage() {
		
		//for now we have Twilio, but can leverage AWS 
		//with a simple uncomment
		ISmsClient client = new SmsClient(smsConfig, smsClient).getSmsClient(); 	

		// build map of post parameters
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.SMS_FROM, outboundTextMessage.getFromPhone());
		params.put(Constants.SMS_TO, outboundTextMessage.getToPhone());
		params.put(Constants.SMS_BODY, outboundTextMessage.getSmsBody());

		Response response;
		try {
			response = client.request(clientUrl, "POST", params);
			if (response == null || response.isError() || response.getHttpStatus() != HttpStatus.OK.value()) {
				LOGGER.info("SmsProcessor: Error send SMS: " + response.getHttpStatus() + "\n"
						+ response.getResponseText());
				outboundTextMessage.setStatus(Constants.SMS_STATUS_UNSENT);
			} else {
				LOGGER.debug("SmsProcessor: Success sending SMS: " + response.getResponseText());
				outboundTextMessage.setStatus(Constants.SMS_STATUS_SENT);
			}
		} catch (Exception e) {
			LOGGER.error("Error sending SMS: ", e);
			outboundTextMessage.setStatus(Constants.SMS_STATUS_UNSENT);
		}
	}
	


    /**
     * Get the content information based off of a content key
     * ie: "CIP_PASS", etc..
     * @param notificationType
     * @return {@link Content}
     * @throws EmailServiceException
     */
    private Content getTemplateContent(String notificationType) throws SmsServiceException {
    	
	        Content content = null;
			try {
				content = smsDao.getTemplateById(notificationType);
			} catch (SQLException e) {
				LOGGER.error("error occured SmsService.getTemplateById()", e);
			}
	        if (content == null) {
	            throw new SmsServiceException("Couldn't retreive Content Node for Repo Key: " + notificationType);
	        }

        return content;
    }

    /**
     * take the content template from the database and
     * dynamically populate it with required values.
     * @param sms a {@link NotificationSms} object
     * @return a string version of the body of the message.
     * @throws SQLException
     */
    private String populateBodyContent(NotificationSms sms) throws SmsServiceException {

       Content content = getTemplateContent(sms.getNotificationType());
        Map<String, String> mappingData = new HashMap<String, String>();
        String document = content.getContentDocument();
        //checking to see if there's any values from the requestValues 
        //if so, add them into the mappingData map
        Set<String> keys = sms.getRequestValues() != null ? sms.getRequestValues().keySet() : null;
        mappingData.put("url_link", content.getLink());
        if (!CollectionUtils.isEmpty(keys)) {
            for (String key : keys) {
                mappingData.put(key, sms.getRequestValues().get(key));
            }
        }
	    document = includeContent(document);
	           
        //taking the document and filling in mapping data and optional content
        document = ContentUtil.updateDocumentContents(document, mappingData);
        
        return document;
  

    }

    /**
     * this method checks for &lt:@include &gt; tags within content
     * if there are tags, then grab the content and insert it into 
     * the document
     * @param document content document to be altered 
     * @return document
     */
    private String includeContent(String document) {
		try {
		     
		     final Matcher includeMatcher = Constants.REGEX_INCLUDE_TAG_PATTERN.matcher(document);		 
		     while(includeMatcher.find()) { //loop through all of the included content sections and populate them
		    	Content content = getTemplateContent(includeMatcher.group(1).trim());
				document = document.replace(includeMatcher.group(0), content.getContentDocument());
		     }
		} catch (SmsServiceException e) {
			LOGGER.error("emailService.includeContent()" + e.getMessage());
		}
		return document;
	}



	@Override
	public OutboundTextMessage getOutboundTextMessage() {
		return outboundTextMessage;
	}

	@Override
	public void setOutboundTextMessage(OutboundTextMessage outboundTextMessage) {
		this.outboundTextMessage = outboundTextMessage;
	}

	@Override
	public List<OutboundTextMessage> getAllSms() {
		List<OutboundTextMessage> smsList = null;
		try {
			smsList = smsDao.getAllSms();
		} catch (SQLException e) {
			LOGGER.error("Error in SmsDao.getAllSms()", e);
		}
		return smsList;
	}


}
