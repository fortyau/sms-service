package net.cyc.service.sms.service.client;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;

import net.cyc.service.sms.domain.Response;
import net.cyc.service.sms.domain.SmsConfig;

public class AwsClient implements ISmsClient {
	
	private Response response;
	private AmazonSNS snsClient;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AwsClient.class);
			
	public AwsClient(SmsConfig config) {
		//TODO determine the configurations and credentials needed
		this.snsClient = AmazonSNSClient.builder()
				.withRegion("us-east-1")
				.build();
	}

	@Override
	public Response request(String url, String arg1, Map arg2) throws Exception {
		// TODO Auto-generated method stub
		try {
		CreateTopicRequest createTopicRequest = new CreateTopicRequest("CYC message topic");
		CreateTopicResult createTopicResult = snsClient.createTopic(createTopicRequest);
		} catch(Exception e) {
			LOGGER.error("AWS sns Service not instanciated", new NullPointerException("No SNS Server has been instanciated"));
		}
		// TODO get the rest of the logic to send SMS via AWS
		return this.getResponse();
	}

	public Response getResponse() {
		return response;
	}

}
