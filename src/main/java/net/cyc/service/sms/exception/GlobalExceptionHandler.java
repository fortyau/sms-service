package net.cyc.service.sms.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    // TODO: Revisit this class once we have a more concrete contract and the BaseExceptionHandler slib finished.
    //       We'll want to be sure to direct specific exceptions around failure to deserialize our request objects
    //       to the appropriate BAD_REQUEST response as well.
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> throwableExceptionHandler(Throwable throwable) {
        LOGGER.error("Throwable caught: ", throwable);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
