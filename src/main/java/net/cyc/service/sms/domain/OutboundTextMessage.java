package net.cyc.service.sms.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import net.cyc.service.sms.controller.serialize.TimeStampJsonSerializer;
import net.cyc.service.sms.controller.serialize.TimestampJsonDeserializer;


public class OutboundTextMessage implements Serializable {

	private static final long serialVersionUID = 1337577707394032209L;
	
	Long id;
	String accountSid;
	String fromPhone;
	String toPhone;
	String smsBody;
	String status;
	String messageType;
    @JsonDeserialize(using = TimestampJsonDeserializer.class)
    @JsonSerialize(using = TimeStampJsonSerializer.class)
	Timestamp sendDate;
    @JsonDeserialize(using = TimestampJsonDeserializer.class)
    @JsonSerialize(using = TimeStampJsonSerializer.class)
	Timestamp dateCreated;
    @JsonDeserialize(using = TimestampJsonDeserializer.class)
    @JsonSerialize(using = TimeStampJsonSerializer.class)
	Timestamp dateUpdated;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccountSid() {
		return accountSid;
	}
	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}
	public String getFromPhone() {
		return fromPhone;
	}
	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}
	public String getToPhone() {
		return toPhone;
	}
	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
	public String getSmsBody() {
		return smsBody;
	}
	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getSendDate() {
		return sendDate;
	}
	public void setSendDate(Timestamp sendDate) {
		this.sendDate = sendDate;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Timestamp getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Timestamp getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Timestamp dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	@Override
	public String toString() {
		return "OutboundTextMessage [id=" + id + ", accountSid=" + accountSid + ", fromPhone=" + fromPhone
				+ ", toPhone=" + toPhone + ", smsBody=" + smsBody + ", status=" + status + ", messageType="
				+ messageType + ", sendDate=" + sendDate + ", dateCreated=" + dateCreated + ", dateUpdated="
				+ dateUpdated + "]";
	}
	
	
}
