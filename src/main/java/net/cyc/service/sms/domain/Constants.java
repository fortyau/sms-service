/**
 * 
 */
package net.cyc.service.sms.domain;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

/**
 * @author Jeff Corbett
 *
 */
public class Constants {
	
	// TODO replace with valid values from the database
	// SMS status values.
	public static final String SMS_STATUS_UNSENT = "UNSENT";
	public static final String SMS_STATUS_SENT = "SENT";
	public static final String SMS_STATUS_VERIFIED = "VERIFIED";
	public static final String SMS_STATUS_RESENT = "RESENT";
	public static final String SMS_STATUS_RESEND = "RESEND";
	public static final String SMS_STATUS_TERMINATE = "TERMINATE";
	public static final String SMS_STATUS_PROCESSED = "PROCESSED";
	public static final String SMS_STATUS_ERROR = "ERROR";
	public static final String SMS_STATUS_HOLD = "HOLD";
	public static final String SMS_STATUS_ABORTED = "ABORTED";
	
    public static final String OPERATION_TAG = "opt";
	
	public static final String SMS_TO= "To";
	public static final String SMS_FROM = "From";
	public static final String SMS_BODY = "Body";
	
	//M_SMS_OUTBOUND.
	public static final String SMSMT_PIN = "SMSMT_PIN"; 
	public static final String SMSMT_SAVE = "SMSMT_SAVE";
	
	
	public static final Pattern REGEX_OPTION_TAG_PATTERN =  Pattern.compile("<"+OPERATION_TAG+"[^>]*>(.*)<\\/"+OPERATION_TAG+"[^>]*>");
	public static final Pattern REGEX_CONDITIONAL_EXPRESSION_PATTERN = Pattern.compile("condition=\"(.*?)\">");
	public static final Pattern REGEX_INCLUDE_TAG_PATTERN = Pattern.compile("<@include(.*?)>");
	
	public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(FORMAT_DATE_TIME).withZone(ZoneOffset.UTC);
	public static final String TWILIO_CLIENT = "twilio";
	public static final String AWS_CLIENT = "aws";
}
