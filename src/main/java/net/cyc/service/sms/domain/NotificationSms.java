/**
 * 
 */
package net.cyc.service.sms.domain;

import java.util.Map;

/**
 * @author Jeff Corbett
 *
 */
public class NotificationSms {

	private String phoneNum;

	private String notificationType;
	private Map<String, String> requestValues;
	
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public Map<String, String> getRequestValues() {
		return requestValues;
	}
	public void setRequestValues(Map<String, String> requestValues) {
		this.requestValues = requestValues;
	}
	@Override
	public String toString() {
		return "NotificationSms [phoneNum=" + phoneNum 
				+ ", notificationType=" + notificationType + ", requestValues=" + requestValues + "]";
	}	

	
}
