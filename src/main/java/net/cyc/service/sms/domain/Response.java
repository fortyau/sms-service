/**
 * 
 */
package net.cyc.service.sms.domain;

/**
 * @author Jeff Corbett
 *
 */
public class Response {

	 
  private String responseText;
  
  private int httpStatus;	  

  private String url;
  
  private String queryString;
  
  private boolean error;

  
	  
	public Response(String responseText, int httpStatus, String url, String queryString) {
		this.responseText = responseText;
		this.httpStatus = httpStatus;
		this.url = url;
		this.queryString = queryString;
	}

	public String getResponseText() {
		return responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	  
	  
	
}
