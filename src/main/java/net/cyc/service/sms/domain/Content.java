package net.cyc.service.sms.domain;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Content implements Serializable {

	private static final long serialVersionUID = 1256334339368937849L;
    public final Logger LOGGER = LoggerFactory.getLogger(Content.class);


    private String contentDocument;
    private String contentSummary;
    private String contentTitle;
    private String link;
    private String contentKey;


    public Content() {
    }
    
    /**
     * Constructor with all of the fields to 
     * populate
     * @param contentDocument
     * @param contentSummary
     * @param contentTitle
     * @param link
     * @param contentKey
     */
    public Content(String contentDocument, String contentSummary, String contentTitle, String link, String contentKey) {
    	this.contentDocument = contentDocument;
    	this.contentSummary = contentSummary;
    	this.contentTitle = contentTitle;
    	this.link = link;
    	this.contentKey = contentKey;
    }

  

    public String getContentKey() {
        return contentKey;
    }

    public void setContentKey(String contentKey) {
        this.contentKey = contentKey;
    }

    public String getContentDocument() {
        return contentDocument;
    }

    public void setContentDocument(String contentDocument) {
        this.contentDocument = contentDocument;
    }

    public String getContentSummary() {
        return contentSummary;
    }

    public void setContentSummary(String contentSummary) {
        this.contentSummary = contentSummary;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

	@Override
	public String toString() {
		return "Content [LOGGER=" + LOGGER + ", contentDocument=" + contentDocument + ", contentSummary="
				+ contentSummary + ", contentTitle=" + contentTitle + ", link=" + link + ", contentKey=" + contentKey
				+ "]";
	}
    
    

}