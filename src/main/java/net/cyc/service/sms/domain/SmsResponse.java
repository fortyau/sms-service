/**
 * 
 */
package net.cyc.service.sms.domain;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jeff Corbett
 *
 */
public class SmsResponse {
	private Boolean success;
	private String message;
	private ObjectMapper mapper;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SmsResponse.class);
	
	public SmsResponse(Boolean success, String message) {
		this.success = Objects.requireNonNull(success, "success cannot be empty");
		this.message = Objects.requireNonNull(message, "message cannot be empty");
		mapper = new ObjectMapper();
	}
	
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * this override will take the object and turn it
	 * into a JSON string
	 * @return a JSON string
	 */
	@Override
	public String toString() {
		String jsonStr = null;
		try {
			jsonStr = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			LOGGER.error(e.getMessage());
		}
		return jsonStr;
	}

}
