package net.cyc.service.sms.domain;

import java.io.Serializable;
import java.util.Date;

public class InboundTextMessage implements Serializable {

	private static final long serialVersionUID = -3557011525145655189L;
	
	Long id;
	String smsSid;
	String accountSid;
	String fromPhone;
	String toPhone;
	String smsBody;
	String fromCity; 
	String fromState; 
	String fromZip; 
	String fromCountry; 
	String toCity; 
	String toState; 
	String toZip; 
	String toCountry; 
	Date smsTimestamp; 
	String status; 
	OutboundTextMessage response;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSmsSid() {
		return smsSid;
	}
	public void setSmsSid(String smsSid) {
		this.smsSid = smsSid;
	}
	public String getAccountSid() {
		return accountSid;
	}
	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}
	public String getFromPhone() {
		return fromPhone;
	}
	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}
	public String getToPhone() {
		return toPhone;
	}
	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
	public String getSmsBody() {
		return smsBody;
	}
	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	public String getFromState() {
		return fromState;
	}
	public void setFromState(String fromState) {
		this.fromState = fromState;
	}
	public String getFromZip() {
		return fromZip;
	}
	public void setFromZip(String fromZip) {
		this.fromZip = fromZip;
	}
	public String getFromCountry() {
		return fromCountry;
	}
	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}
	public String getToCity() {
		return toCity;
	}
	public void setToCity(String toCity) {
		this.toCity = toCity;
	}
	public String getToState() {
		return toState;
	}
	public void setToState(String toState) {
		this.toState = toState;
	}
	public String getToZip() {
		return toZip;
	}
	public void setToZip(String toZip) {
		this.toZip = toZip;
	}
	public String getToCountry() {
		return toCountry;
	}
	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}
	public Date getSmsTimestamp() {
		return smsTimestamp;
	}
	public void setSmsTimestamp(Date smsTimestamp) {
		this.smsTimestamp = smsTimestamp;
	}
	public OutboundTextMessage getResponse() {
		return response;
	}
	public void setResponse(OutboundTextMessage response) {
		this.response = response;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
