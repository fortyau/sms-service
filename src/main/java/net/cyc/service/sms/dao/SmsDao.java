/**
 * 
 */
package net.cyc.service.sms.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import net.cyc.service.sms.dao.SmsDao.ContentRowMapper;
import net.cyc.service.sms.domain.Content;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.domain.SmsConfig;

/**
 * @author Jeff Corbett
 *
 */
public class SmsDao implements ISmsDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public final Logger LOGGER = LoggerFactory.getLogger(SmsDao.class);
	
	private final String SELECT_SMS_CONFIG_QUERY = "SELECT * FROM SMS_CONFIG WHERE ID = ?";
	
	private final String INSERT_QUERY = "INSERT INTO SMS_OUTBOUNDS (ACCOUNT_SID,FROM_PHONE,TO_PHONE,SMS_BODY, "
			+ "							SEND_STATUS, SEND_DATE, MESSAGE_TYPE_ID, SMS_OUTBOUND_CREATED, SMS_OUTBOUND_MODIFIED) VALUES"
										+ "(?,?,?,?,?,?,(SELECT MESSAGE_TYPE_ID FROM MESSAGE_TYPES WHERE MESSAGE_TYPE_NAME= ?),?,?)" ;

	private final String SELECT_QUERY = "SELECT o.ID,"
			+ " o.ACCOUNT_SID,"
			+ " o.FROM_PHONE,"
			+ " o.TO_PHONE,"
			+ " o.SMS_BODY,"
			+ " o.SEND_STATUS,"
			+ " o.SEND_DATE,"
			+ " o.SMS_OUTBOUND_CREATED,"
			+ " o.SMS_OUTBOUND_MODIFIED,"
			+ " mt.MESSAGE_TYPE_NAME FROM SMS_OUTBOUNDS o"
			+ " JOIN MESSAGE_TYPES mt ON "  
			+ " mt.MESSAGE_TYPE_ID = o.MESSAGE_TYPE_ID"
			+ " ORDER BY ID";
	
    private final String GET_TEMPLATE_BY_KEY_QUERY = "SELECT content_id, content_key, "
            + "content_document,content_summary, content_link FROM contents WHERE content_key = ?";

	
	public SmsDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * This method takes an ID and pulls the configuration from
	 * the database.
	 * @param Long Id of the configuration 
	 * @throws SQLException
	 */
	@Override
	public SmsConfig getSmsConfiguration(Long id) throws SQLException {
        SmsConfig smsConfig = new SmsConfig();
        smsConfig = jdbcTemplate.queryForObject(SELECT_SMS_CONFIG_QUERY, new Object[] {id}, new RowMapper<SmsConfig>() {
            public SmsConfig mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            	SmsConfig sms = new SmsConfig();
                sms.setId((long) resultSet.getInt("id"));
                sms.setName(resultSet.getString("name"));
                sms.setAccountSid(resultSet.getString("account_sid"));
                sms.setAuthToken(resultSet.getString("auth_token"));
                sms.setCallerId(resultSet.getString("caller_id"));
                sms.setApiVersion(resultSet.getString("api_version"));
                
                return sms;

            }
        });
		return smsConfig;
	}

	/**
	 * take the {@link OutboundTextMessage} and persist it out to 
	 * the database.
	 * 
	 * @param {@link OutboundTextMessage} 
	 * @throws SQLException
	 */
	@Override
	public void persistOutboundMessage(OutboundTextMessage outboundTextMessage) {
        final OutboundTextMessage smsMsg = outboundTextMessage;
        
	        jdbcTemplate.update(INSERT_QUERY, new PreparedStatementSetter() {
	        java.sql.Timestamp sqlDate = new java.sql.Timestamp(System.currentTimeMillis());
	            @Override
	            public void setValues(PreparedStatement ps) throws SQLException {
	                ps.setString(1, smsMsg.getAccountSid());
	                ps.setString(2, smsMsg.getFromPhone());
	                ps.setString(3, smsMsg.getToPhone());
	                ps.setString(4, smsMsg.getSmsBody());
	                ps.setString(5, smsMsg.getStatus());
	                ps.setTimestamp(6, smsMsg.getSendDate());
	                ps.setString(7, smsMsg.getMessageType());
	                ps.setTimestamp(8, sqlDate);
	                ps.setTimestamp(9, sqlDate);
	            }
	
	        });

		
	}


    @Override
    public List<OutboundTextMessage> getAllSms() throws SQLException {
        List<OutboundTextMessage> correspondenceList = new ArrayList<OutboundTextMessage>();
        correspondenceList = jdbcTemplate.query(SELECT_QUERY, new RowMapper<OutboundTextMessage>() {
            public OutboundTextMessage mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            	OutboundTextMessage sms = new OutboundTextMessage();
                sms.setId((long) resultSet.getInt("id"));
                sms.setAccountSid(resultSet.getString("ACCOUNT_SID"));
                sms.setToPhone(resultSet.getString("TO_PHONE"));
                sms.setFromPhone(resultSet.getString("FROM_PHONE"));
                sms.setSendDate(resultSet.getTimestamp("SEND_DATE"));
                sms.setSmsBody(resultSet.getString("SMS_BODY"));                
                sms.setMessageType(resultSet.getString("MESSAGE_TYPE_NAME"));
                sms.setStatus(resultSet.getString("SEND_STATUS"));
                sms.setDateCreated(resultSet.getTimestamp("SMS_OUTBOUND_CREATED"));
                sms.setDateUpdated(resultSet.getTimestamp("SMS_OUTBOUND_MODIFIED"));
                return sms;

            }
        });
        return correspondenceList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Content getTemplateById(String key) throws SQLException {
    	try {
        Content content = jdbcTemplate.queryForObject(GET_TEMPLATE_BY_KEY_QUERY, new Object[] { key },
                new ContentRowMapper());
	        return content;
    	} catch (Exception e) {
    		LOGGER.error("error emailDao.getTemplateById()" + e.getMessage());
    	}
    	return null;
    }

    public class ContentRowMapper implements RowMapper<Content> {
        public Content mapRow(ResultSet rs, int rowNum) throws SQLException {
            Content content = new Content();
            content.setContentDocument(rs.getString("content_document"));
            content.setContentKey(rs.getString("content_key"));
            content.setContentSummary(rs.getString("content_summary"));
            content.setLink(rs.getString("content_link"));
            return content;
        }

    }



}
