package net.cyc.service.sms.dao;

import java.util.List;
import java.sql.SQLException;

import net.cyc.service.sms.domain.Content;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.domain.SmsConfig;

public interface ISmsDao {
	
	public SmsConfig getSmsConfiguration(Long id) throws SQLException;

	public void persistOutboundMessage(OutboundTextMessage outboundTextMessage) throws SQLException;
	
	public List<OutboundTextMessage> getAllSms() throws SQLException;

	/**
	 * Pass an ID to the DAO and get the 
	 * corresponding template
	 * @param notificationType
	 * @return
	 */
	public Content getTemplateById(String notificationType) throws SQLException;

}
