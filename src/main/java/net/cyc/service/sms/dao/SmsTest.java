/**
 * 
 */
package net.cyc.service.sms.dao;

import java.rmi.RemoteException;

import org.springframework.jdbc.core.JdbcTemplate;

import net.cyc.service.sms.domain.Constants;
import net.cyc.service.sms.domain.NotificationSms;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.domain.SmsConfig;
import net.cyc.service.sms.service.ISmsService;
import net.cyc.service.sms.service.SmsService;

/**
 * @author Jeff Corbett
 *
 */
public class SmsTest {

	
	private static ISmsDao dao;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ISmsService service = new SmsService(dao,"","twilio");
		
		try {
			sendSmsPin("send code");
			NotificationSms sms = new NotificationSms();
			
			//service.execute(sms);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static void sendSmsPin(String MfaCode) throws Exception, RemoteException {
		//Saving text message and send text auth message to mobile phone number.		
		String act_msg = "Your Identification Code is " + MfaCode +".  Please enter this code after you log into your account to register your computer. Msg&Data rates may apply.";
		dao = new SmsDao(new JdbcTemplate());
		OutboundTextMessage txtMsg = new OutboundTextMessage();
		txtMsg.setToPhone("6155551212");
		txtMsg.setMessageType(Constants.SMSMT_PIN);
		txtMsg.setSmsBody(act_msg);

		SmsConfig sms = new SmsConfig();
		sms =  dao.getSmsConfiguration(sms.getId());		
		txtMsg.setFromPhone(sms.getCallerId());
		
		SmsService smsService = new SmsService(dao,"", "twilio");
		smsService.setOutboundTextMessage(txtMsg);
	//	bcb.executeSingle(smsService);
	}

}
