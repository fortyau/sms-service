package net.cyc.service.sms.configuration;

import net.cyc.service.sms.dao.SmsDao;
import net.cyc.service.sms.dao.ISmsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

//@Configuration
public class DaoConfiguration {
    @Value("${dao.datasource.driver-class-name}")
    private String driverClassName;

    @Value("${dao.datasource.url}")
    private String url;

    @Value("${dao.datasource.username}")
    private String username;

    @Value("${dao.datasource.password}")
    private String password;

    @Bean
    public DataSource awsPostgresqlDataSource() {
        final DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driverClassName);
        dataSourceBuilder.username(username);
        dataSourceBuilder.password(password);
        dataSourceBuilder.url(url);
        final DataSource dataSource = dataSourceBuilder.build();
        return dataSource;
    }

    @Autowired
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Autowired
    @Bean
    public ISmsDao smsDao(JdbcTemplate jdbcTemplate) {
        final SmsDao smsDao = new SmsDao(jdbcTemplate);
        return smsDao;
    }
}
