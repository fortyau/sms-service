/**
 * 
 */
package net.cyc.service.sms.configuration;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import net.cyc.service.sms.dao.SmsDao;
import net.cyc.service.sms.dao.ISmsDao;

/**
 * Embedded datasource. This is temporary. once access to JDBC is setup then
 * this config will be turned off
 * 
 * @author Jeff Corbett
 *
 */

@Configuration
public class DaoConfigDerby {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaoConfigDerby.class);

    @Bean
    public DataSource dataSource() throws SQLException {
        final EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
        final DataSource dataSource = embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.DERBY)
                .addScript("create_test_database.sql").build();
        return dataSource;
    }

    @Autowired
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Autowired
    @Bean("SmsDao")
    public ISmsDao smsDao(JdbcTemplate jdbcTemplate) {
        final ISmsDao smsDao = new SmsDao(jdbcTemplate);
        return smsDao;
    }
}
