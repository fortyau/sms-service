package net.cyc.service.sms.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.cyc.service.sms.dao.ISmsDao;
import net.cyc.service.sms.service.ISmsService;
import net.cyc.service.sms.service.SmsService;

/**
 * @author Jeff Corbett
 *
 */
@Configuration
public class SmsConfig {
	
	@Value ("${sms.clientUrl}")
	private String clientUrl;
	
	@Value ("${sms.smsClient}")
	private String smsClient;

	@Autowired
	@Bean
	public ISmsService smsService(ISmsDao smsDao) {
		final ISmsService smsService = new SmsService(smsDao, clientUrl, smsClient);
		return smsService;
	}
		
}
