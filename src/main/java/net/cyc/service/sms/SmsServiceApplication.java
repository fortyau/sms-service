package net.cyc.service.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SmsServiceApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SmsServiceApplication.class);
	
	public static void main(String[] args) {
		try {
			SpringApplication.run(SmsServiceApplication.class, args);
		}catch(Exception e) {
			LOGGER.error("failed to stat service", e);
			System.exit(1);
		}
	
	}
}
