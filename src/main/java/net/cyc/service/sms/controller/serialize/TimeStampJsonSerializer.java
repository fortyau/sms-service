package net.cyc.service.sms.controller.serialize;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import net.cyc.service.sms.domain.Constants;

public class TimeStampJsonSerializer extends JsonSerializer<Timestamp> {

	@Override
	public void serialize(Timestamp timestamp, JsonGenerator jsonGenerator, SerializerProvider arg2)	throws IOException, JsonProcessingException {
		Instant instant = timestamp.toInstant();
        final String string = Constants.DATE_TIME_FORMATTER.format(instant);
        jsonGenerator.writeString(string);
		// TODO Auto-generated method stub
		
	}

}
