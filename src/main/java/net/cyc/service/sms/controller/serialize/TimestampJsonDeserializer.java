/**
 * 
 */
package net.cyc.service.sms.controller.serialize;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.cyc.service.sms.domain.Constants;

/**
 * @author Jeff Corbett
 *
 */
public class TimestampJsonDeserializer extends JsonDeserializer<Timestamp> {
	
    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    	Instant instant = Instant.from(Constants.DATE_TIME_FORMATTER.parse(jsonParser.getText()));
        return Timestamp.from(instant);
    }

}
