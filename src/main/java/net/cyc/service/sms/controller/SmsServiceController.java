package net.cyc.service.sms.controller;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.cyc.service.sms.domain.NotificationSms;
import net.cyc.service.sms.domain.OutboundTextMessage;
import net.cyc.service.sms.service.ISmsService;

@RestController
@RequestMapping("/service/sms")
public class SmsServiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsServiceController.class);
    private ISmsService smsService;

    @Autowired
    public SmsServiceController(ISmsService smsService) {
        this.smsService = Objects.requireNonNull(smsService, "service cannot be empty");
    }

    /**
     * takes a sms request and sends it out to be processed.
     * 
     * @param sms
     * @return a http status of 200
     */
    @PostMapping(value = "/send", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String runSms(@RequestBody NotificationSms sms) {
    	Objects.requireNonNull(sms, "Sms Notification cannot be empty");
    	Objects.requireNonNull(sms.getNotificationType(), "Notification type cannot be empty.");
    	Objects.requireNonNull(sms.getPhoneNum(), "Recipients phone cannot be empty.");
        return this.smsService.sendSmsMessage(sms);        

    }

    /**
     * Currently, this will be a placeholder for a future story where one can query
     * the text messages that have been received. once completed it will get a listing of
     * text messages based on one of the the following: user, phone, email, date
     * 
     * @return a list of sms that have been sent out
     */
    @GetMapping(value = "/getall")
    @ResponseBody
    public List<OutboundTextMessage> retrieveNotifications() {

        return this.smsService.getAllSms();
    }
}
