/**
 * 
 */
package net.cyc.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.lang.text.StrSubstitutor;

import net.cyc.service.util.Constants;

/**
 * This class will take a content template and 
 * dynamically determine how and with what to populate
 * the template.
 * @author Jeff Corbett
 *
 */
public class ContentUtil {
	
	/**
	 * This method takes a string along with a map of 
	 * data that should match to variable fields within the
	 * template. It will also determine if there are any optional
	 * areas of the template that may or may not be included based on
	 * embedded conditionals. The syntax for the tag is as follows:<br/>
	 * <b>&lt;opt condition="[operand1 operator operand2]" &gt;[optional text] &lt;/opt&gt;</b> for example:<br/>
	 * &lt;opt condition="tot_amt GT 100"&gt; test text &lt;/opt&gt;<br/>
	 * will include the text "test text" if there is a tot_amt key within
	 * the map and the value of that map is greater than 100. If not, then
	 * the text within the tag will be omitted. <br/>
	 * Currently, the only operators allowed are [GT,LT,EQ,NEQ] see {@link Operators}
	 * 
	 * @param document the template document to be populated
	 * @param mappingData a {@link HashMap} that contains values to populate the document with
	 * @return the populated document
	 */
	 public static String updateDocumentContents(String document, Map<String, String> mappingData) {

	     final Matcher optionalMatcher = Constants.REGEX_OPTION_TAG_PATTERN.matcher(document);
	     
	     while (optionalMatcher.find()) { // loop through all of the conditional sections and apply logic
	    	 Matcher condMatcher = Constants.REGEX_CONDITIONAL_EXPRESSION_PATTERN.matcher(optionalMatcher.group(0));
	    	 while(condMatcher.find()) {
	    		 String conditionalStr = condMatcher.group(1);
	    		 conditionalStr =conditionalStr.replaceAll("\\s+", " ");
	    		 String[] operands = condMatcher.group(1).split(" ");
	    		 String operand1 = getOperandValue(operands[0], mappingData);
	    		 String operand2 = getOperandValue(operands[2], mappingData);
	    		 
	    		 //this operator.evaluate will take the two operands and 
	    		 // performed the requested evaluation on them returning
	    		 // a bool. if true, keep the optional text in, otherwise, 
	    		 // remove the tags, and keep the content.
	    		 Operators operator = Operators.valueOf(operands[1].toUpperCase());
	    		 if(!operator.evaluate(operand1, operand2)) {	    			 
	    			 document = document.replace(optionalMatcher.group(0), ""); // evaluation failed, omit section
	    		 } else {
	    			 document = document.replace(optionalMatcher.group(0), optionalMatcher.group(1)); // evaluation passed, include section
	    		 }	    		 
	    	 }	         
	     }
	    document = StrSubstitutor.replace(document, mappingData);
	    return document;
	     
	 }

	 /**
	  * Take the value of the operand determine if it's part of the 
	  * mappedData, if so, get that value. if not, then it is either a
	  * string or a number. just return.
	  * @param string value to see if it's a Map key
	  * @param mappingData a {@link HashMap} of Key/Value pairs 
	  * @return string representation of the actual value
	  */
	private static String getOperandValue(String operand, Map<String, String> mappingData) {
		for(String key : mappingData.keySet()) {
			if(key.equalsIgnoreCase(operand)) {
				return mappingData.get(key);
			}
		}
		return operand;
	}
}
