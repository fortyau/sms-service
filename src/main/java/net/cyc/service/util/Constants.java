/**
 * 
 */
package net.cyc.service.util;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

/**
 * @author Jeff Corbett
 *
 */
public class Constants {
	
    public static final String OPERATION_TAG = "opt";
    
	public static final Pattern REGEX_OPTION_TAG_PATTERN =  Pattern.compile("<"+OPERATION_TAG+"[^>]*>(.*)<\\/"+OPERATION_TAG+"[^>]*>");
	public static final Pattern REGEX_CONDITIONAL_EXPRESSION_PATTERN = Pattern.compile("condition=\"(.*?)\">");

	public static final Pattern REGEX_INCLUDE_TAG_PATTERN = Pattern.compile("<@include(.*?)>");

}
