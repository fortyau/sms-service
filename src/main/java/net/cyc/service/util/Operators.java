package net.cyc.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this enum is used to process evaluations of values that 
 * can be string or numeric
 * 
 * @author Jeff Corbett
 *
 */
public enum Operators {

	GT,  //Greater Than
	LT,  //Less Than
	EQ,  //Equal To
	NEQ; //Not equal To
	
	 public final Logger LOGGER = LoggerFactory.getLogger(Operators.class);
	
	/**
	 * Dependent on the type of operator chosen, evaluate with
	 * the proper method
	 * @param operand1 Left half of the evaluation
	 * @param operand2 Right half of the evaluation
	 * @return boolean dependent on how the two evaluate
	 */
	public Boolean evaluate(String operand1, String operand2) {
		
		switch (this) {
			case GT:
				return evaluateGreaterthan(operand1, operand2);
			case LT:
				return evaluateLessthan(operand1, operand2);
			case EQ:
				return evaluateEquals(operand1, operand2);
			case NEQ:
				return evaluateNotequals(operand1, operand2);
			default:
				throw new AssertionError("Invalid operation " + this);
		}
	}

	private Boolean evaluateEquals(String operand1, String operand2) {
		
		if(isNumeric(operand1) && isNumeric(operand2)){
			return evaluateEquals(Float.valueOf(operand1), Float.valueOf(operand2));
		}
		return operand1.equalsIgnoreCase(operand2);
	}

	private Boolean evaluateEquals(Float operand1, Float operand2) {
		return operand1 == operand2;
	}

	private Boolean evaluateLessthan(String operand1, String operand2) {
		if(isNumeric(operand1) && isNumeric(operand2)){
			return (((float)(Float.valueOf(operand1)) < (float)Float.valueOf(operand2)));
		}
		return false;
	}

	private Boolean evaluateGreaterthan(String operand1, String operand2) {
		if(isNumeric(operand1) && isNumeric(operand2)){
			return Float.valueOf(operand1) > Float.valueOf(operand2);
		}
		return false;
	}
	
	private Boolean evaluateNotequals(String operand1, String operand2) {
		if(isNumeric(operand1) && isNumeric(operand2)){
			return evaluateNotequals(Float.valueOf(operand1), Float.valueOf(operand2));
		}
		return !operand1.equalsIgnoreCase(operand2);
	}

	private Boolean evaluateNotequals(Float operand1, Float operand2) {
		
		return operand1 != operand2;
	}
	
	/**
	 * Method takes a String and tries to parse the
	 * value. if it throws an exception, then the String
	 * is not a number.
	 * @param strValue the value we try to parse value
	 * @return whether or not the value can be parsed.
	 */
	private  Boolean isNumeric(String strValue) {
		try {
			Float.parseFloat(strValue);
			return true;
		} catch (Exception e) {
			LOGGER.warn("bad number:" + strValue);
			
		}
		return false;
	}
}
